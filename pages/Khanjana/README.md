<h1>Welcome to my Workshop</h1>

### Hello, there I'm Khanajana!

<details>
  <summary>list of contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
         <li><a href="#usage">Usage</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#git-command-guide">Git Command Guide</a>
    <ul>
        <li><a href="#clone-repository-and-connect">Clone Repository and Connect</a></li>
        <li><a href="#create-and-checkout-new-branch">Create and Checkout New Branchn</a></li>
         <li><a href="#commit-and-push-changes">Commit and Push Changes</a></li>
         <li><a href="#pull-changes-from-upstream">Pull Changes from Upstream</a></li>
      </ul>
    </li>
    <li><a href="#project-structure-guide">Project Structure Guide</a></li>
    <li><a href="#my-project-structure-guide">My Project Structure Guide</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

As a co-op student, my assignment focused on learning, practicing, and engaging in workshops to enhance my skills. Throughout this journey, I've ventured into various workshops, honing my abilities using **Nuxt 3.js**

### All My Workshop :

- **Workshop 1 My Proflie**, Crafting a comprehensive profile incorporating essential details like my name, job position, skills, and hobbies.

- **Workshop 2 CRUD Operations**, Retrieve data through a free REST API that provides mock data from melivecode.com, utilizing methods such as GET, POST, and DELETE.

- **Workshop 3 Website Development**, Developing a website page according to specified design guidelines.

- **Workshop 4 Authentication & Middleware**, Implementing login, authentication, and middleware functionalities.

Each workshop served as a significant learning experience, providing insights into practical application and problem-solving using NUXT.js. The journey through these workshops equipped me with a deeper understanding of web development principles and their real-world applications!

### Built With

The project was constructed using a versatile set of technologies and tools that contributed to its development:

- [![Nuxt.js]][Nuxt-url]
- [![Vue.js]][Vue-url]
- [![Vuetify3]][Vuetify3-url]
- [![gitlab]][gitlab-url]

- [![pictogrammers]][pictogrammers-url]

<!-- GETTING STARTED -->

## Getting Started

To begin with, follow these steps to get started with the project :

### Prerequisites

**Ensure you have the following installed :**

- Node.js - Node.js runtime environment
- Npm or Yarn - Package managers for Node.js

### Installation

1. Install dependencies:
   ```sh
   npm install
    # or
   yarn install
   ```
2. Start the development server:
   ```sh
   npm run dev
    # or
   yarn dev
   ```

<!-- USAGE -->

### Usage

This will start the development server and make the project available at http://localhost:3000.

## How to Access the Application

Upon opening the project, you'll encounter a login page.

**To proceed, use the following credentials:**

- **Username:** `karn.yong@melivecode.com`
- **Password:** `melivecode`

These credentials are mock data obtained from the `https://melivecode.com/` [Learn More](https://melivecode.com/) about the API used in this project.

> **Note :** These credentials are for demonstration purposes only and do not represent actual user information.

<p align="right"><a href="#about-the-project">back to top</a></p>

<!-- CONTRIBUTING -->

## Contributing

If you're interested in contributing to this project, we welcome your ideas and input! Here's how you can contribute:

- Fork the repository.
- Make your changes in a new branch: `git checkout -b <branch-name>`
- Commit your changes: `git commit -m 'Add your message here'`
- Push to the branch: `git push origin <branch-name>`
- Create a new Pull Request.

For major changes, please open an issue first to discuss what you would like to change.

## Git Command Guide

Here's a quick reference guide for some commonly used Git commands:

### Clone Repository and Connect

```bash
git clone <my-link-to-github-remote-repo>
cd <folder-name>
git remote add upstream <link-to-github-remote-repo>
git remote -v
```

**Explanation :**

- `git clone <my-link-to-github-remote-repo>`: Clone the repository from your GitHub remote link to your local machine.
- `cd <folder-name>`: Navigate into the newly created folder.
- `git remote add upstream <link-to-github-remote-repo>`: Connect the upstream repository (typically the original repository) from the provided link.
- `git remote -v`: Verify the remote repositories connected to your local repository.

### Create and Checkout New Branch

```bash
git branch <Branch-Name>     # Create a new branch
git checkout <Branch-Name>   # Switch to the created branch
```

**Explanation :**

- `git branch <Branch-Name>`: Create a new branch with the specified name.
- `git checkout <Branch-Name>`: Switch to the newly created branch to start working on it.

### Commit and Push Changes

```bash
git add .                     # Add changes to the staging
git commit -m "Commit message"  # Commit changes with a descriptive message
git push origin <Branch-Name> # Push changes to the remote repository
```

**Explanation :**

- `git add .`: Stage all changes (addition, modification, deletion) to be committed.
- `git commit -m "Commit message"`: Commit staged changes with a descriptive message explaining the changes made.
- `git push origin <Branch-Name>`: Push committed changes to the specified branch on the remote repository (origin).

> **Note :** `After pushing the changes to your branch, visit your GitLab repository. Select the branch you pushed to and click on` **"Create Merge Request"** `to initiate the merge request process.`

### Pull Changes from Upstream

```bash
git checkout main          # Switch to the main branch
git pull upstream main      # Pull the latest changes from the upstream
```

**Explanation :**

- `git checkout main` : Switch to the main branch
- `git pull upstream main` : Pull the latest changes from the upstream repository (the original repository) to your local main branch.
<p align="right"><a href="#about-the-project">back to top</a></p>

## Project Structure Guide

```bash
project-root/
│
├── nuxt/
│   └── ... (Nuxt.js related files and configurations)
│
├── assets/
│   ├── images/
│   ├── styles/
│   └── ...
│
├── components/
│   ├── dashboard
│   ├── ui-components
│   └── ...
│
├── layouts/
│   ├── blank.vue
│   └── default.vue
│
│
├── middleware/
│   └─── auth.js
│
├── pages/
│   ├── Khanjana/ #this my workshop
│   ├── # My teammates workshop
│   └── ...
│
├── .env/
│
├── app.vue
│
├── nuxt.config.ts
│
└── ...
```

**Explanation :**

- `nuxt/` : Contains files and configurations related to the Nuxt.js framework or settings used to build the application with Nuxt.js.
- `assets/` : Stores various static resources such as images, stylesheets, and other related files used in the project.
- `components/` : Houses reusable Vue components or modules that can be used across the project.
- `layouts/` : Stores layout files defining the main structure of different web pages, such as a default layout for general pages or a specific layout for news-related pages.
- `middleware/` : Holds middleware files used for pre-processing data before Nuxt.js renders web pages.
- `pages/` : Contains Vue components used to display different web pages of the project
- `.env/` : Contains environment configuration files
- `app.vue` : This file represents the root component of your Nuxt.js application. It wraps all content within the application and is responsible for rendering other components and layouts.

## My Project Structure Guide

```bash
pages/
  │
  └──Khanjana/
        │
        ├── appComponent/
        │   ├── dashboardstorageComp.vue/
        │   ├── editComp.vue
        │   ├── logout.vue
        │   ├── searchDataComp.vue
        │   └── ...
        │
        ├── image/
        │   └── # my profile image
        │
        ├── Khanjana.vue # workshop 1 My profile.
        │
        │
        ├── login.vue # workshop 4 Implementing login.
        │
        │
        ├── workshop2.vue # CRUD Operations.
        │
        │
        ├── workshop3.vue # Website Development.
        │
        │
        └── myWorkshop.vue # display all workshop options

```

**Explanation :**

- `Khanjana/` : This is the main folder for your project.
- `appComponent/` : Contains files for various components used in the project, like dashboardstorageComp.vue, editComp.vue, logout.vue, searchDataComp.vue, etc.
- `image/` : Stores image files, such as your profile image or other images used in the project.
- `Khanjana.vue` : Represents Workshop 1, which is the "My Profile" page.
- `login.vue` : Represents Workshop 4, which focuses on "Implementing Login".
- `workshop2.vue` : Represents Workshop 2, dealing with CRUD Operations.
- `workshop3.vue` : Represents Workshop 3, related to "Website Development".
- `myWorkshop.vue` : A component used to display all workshop options available.

<!-- CONTACT -->

## Contact

You can contact me through the following channels :

[![Gmail][Gmail-shield]][Gmail-url]

[![Facebook][Facebook-shield]][Facebook-url]

<h2 align="center"></h2>

<p align="right"><a href="#about-the-project">back to top</a></p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[Gmail-shield]: https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white
[Gmail-url]: mailto:unlucky00123@gmail.com
[Facebook-shield]: https://img.shields.io/badge/Facebook-1877F2?style=for-the-badge&logo=facebook&logoColor=white
[Facebook-url]: https://www.facebook.com/profile.php?id=100007978217906
[Nuxt.js]: https://img.shields.io/badge/nuxt.js-000000?style=for-the-badge&logo=nuxtdotjs
[Nuxt-url]: https://nuxt.com/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs
[Vue-url]: https://vuejs.org/
[Vuetify3]: https://img.shields.io/badge/vuetify-1867C0?style=for-the-badge&logo=vuetify&logoColor=white
[Vuetify3-url]: https://vuetifyjs.com/en/getting-started/installation/
[JavaScript]: https://img.shields.io/badge/javascript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black
[gitlab]: https://img.shields.io/badge/gitlab-FCA121?style=for-the-badge&logo=gitlab&logoColor=white
[gitlab-url]: https://about.gitlab.com/
[pictogrammers]: https://img.shields.io/badge/material_design_icons-2196F3?style=for-the-badge&logo=material-design-icons&logoColor=white
[pictogrammers-url]: https://pictogrammers.com/library/mdi/
