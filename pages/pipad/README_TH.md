# FOR THAI PLEASE PREFERS TO FILE README_TH

## แนะนำตัว 
นี่คือโปรเจค Nuxt ของผมที่ใช้ Dependency และ Framework มาเพื่อสร้างเว็บแอพพลิเคชั่พที่มีความ progressive

โปรเจคนี้ประกอบด้วย

1. Workshop Resume 
    - เป็น workshop ที่เก็บข้อมูลส่วนตัวเช่น ชื่อ ชื่อเล่น และ ช่องทางติดต่อ
2. Workshop CRUD
    - เป็น workshop ที่ดึงค่ามาจาก API ภายนอกและนำข้อมูลมาแสดงในตาราง สามารถทำฟังค์ชั่นพื้นฐานเช่นการเพิ่ม ลบ แก้ไข และมีฟีเจอร์ในการ ค้นหา และแยก ข้อมูลมาแสดงที่ละหน้าด้วยการ Pagination
3. Workshop Dashboard
    - เป็น Workshop ที่นำ component ต่างๆมาแสดงภายในหน้าหลักเพียงหน้าเดียว และมีความสามารถในการ emit และ prop ข้อมูลละหว่าง parent และ child ได้ และนำข้อมูลมาเก็บไว้ภายใน localStorage.
4. Workshop Login
    - เป็น workshop ที่ทำการ login ด้วยการใช้ email และ password เพื่อนำ accessToken มาเก็บไว้ใน localStorage และนำไปตรวจสอบด้วยการส่งไปยัง API 


## การติดตั้ง
ขั้นตอนสำหรับการติดตั้ง Dependencies ก่อนทำการเปิดเซิฟเวอร์

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```


## การเปิดเว็บเซิฟเวอร์
คำสั่งสำหรับการเปิดเว็บเซิฟเวอร์บน http://localhost:3000

```bash
npm run dev
```


## Framework and Dependency
Framework และ Dependency ที่นำมาใช้ในโปรเจคนี้

1. [Nuxt](https://nuxt.com/docs/getting-started/installation)
    - Nuxt เป็น Framework สำหรับ Vue.js ที่ให้การแยกส่วนในระดับสูงสำหรับการสร้างเว็บแอปพลิเคชันและมีฟีเจอร์ที่หลายหลาก
2. [Vuetify](https://vuetifyjs.com/en/introduction/why-vuetify/#what-is-vuetify3f)
    - เป็น Library สำหรับนำ Component สำเร็จรูปมาใช้ภายในเว็บแอพพลิเคชั่น
3. [Fontawesome](https://fontawesome.com/docs)
    - เป็น Library สำหรับนำ Icon และ Font มาตกแต่งใช้ในเว็บแอพพลิเคชั่น
4. [mdi](https://pictogrammers.com/library/mdi/)
    - เป็น Library สำหรับนำ Icon มาตกแต่งใช้ในเว็บแอพพลิเคชั่น
5. [chart.js](https://www.chartjs.org/docs/latest/)
    - เป็น Library สำหรับการแสดงข้อมูลออกมาในรูปแบบของ 
6. [eslint](https://eslint.org/docs/latest/)
    - เป็น linting tool สำหรับ JavaScript เพื่อตรวจหาข้อผิดพลาดภายในตัวโค้ด รวมถึงการแก้ไข error ภายใน code
7. [prettier](https://prettier.io/docs/en/)
    - เป็นตัว formatter สำหรับ JavaScript เพื่อเพิ่มความสวยงามสำหรับตัวโค้ด
8. [sass](https://sass-lang.com/documentation/)
    - sass เป็นส่วนขยาย ของ CSS ที่เพิ่มความสามารถบางอย่างลงไป เพื่อแก้ไขปัญหาต่างๆ ของการเขียน CSS code ในรูปแบบเดิม
9. [vee-validate](https://vee-validate.logaretm.com/v4/guide/overview/)
    - vee-validate เป็น Library เพื่อตรวจสอบความถูกต้องของ form เช่น Email ต้องมี @ และ comma 


## Nuxt 3 Minimal Starter
[เอกสารประกอบ](https://nuxt.com/docs/getting-started/introduction) สำหรับการเริ่มต้นใช้งาน Nuxt 3


## Vuetify Starter
[เอกสารประกอบ](https://vuetifyjs.com/en/getting-started/installation/) สำหรับการเริ่มต้นใช้งาน Vuetify


## Environment Variables
ในโปรเจ็คนี้ได้มีการใช้ตัวแปร environment เพื่อสำหรับเก็บค่า URL ไว้ โดยหลักมี
- **URL_Auth** เป็นตัวแปรสำหรับเก็บ URL ที่ใช้สำหรับเรียกการใช้ API ภายในโปรเจค


## File Directory and File Guide
>ก่อนการเข้าถึงหน้าต่างๆที่ต้องการ ต้องทำการเข้าสู่ระบบด้วย Email และ Password ด้านล่างผ่านหน้า Login Page ก่อนเสมอ
> Email : karn.yong@melivecode.com
> Password : melivecode

**ตำแหน่งของไฟล์และคำอธิบายไฟล์**
>หากต้องการดูคำอภิบายของแต่ละบรรทัดภายในไฟล์โปรดอ่าน comment ภายในไฟล์

Crud Table
```bash
|-- pages/
|   |-- pipad/
|       |-- crud_pipad.vue
#ไฟล์นี้คือไฟล์ที่แสดงและเพิ่มลบแก้ไขข้อมูลจาก API melivecode
#https://www.melivecode.com/   <-- API url
``` 

Login Page
```bash
|-- pages/
|   |-- pipad/
|       |-- pipad_login.vue
#ไฟล์นี้คือไฟล์ที่แสดงข้อมูลส่วนตัวของผู้สร้าง
```

Resume Page
```bash
|-- pages/
|   |-- pipad/
|       |-- Resume.vue
#ไฟล์นี้คือไฟล์ที่แสดงข้อมูลส่วนตัวของผู้สร้าง
```
Dashboard Page
```bash
|-- pages/
|   |-- pipad/
|       |-- work5.vue
#ไฟล์นี้คือหน้า Template หลักที่นำ Component จาก components/pipadcombo มาแสดง
```

Dashboard Page Component
```bash
|-- components/
|   |-- pipadcompo/
|   |-- |-- activity.vue
|   |-- |-- channel.vue
|   |-- |-- dashboard.vue
|   |-- |-- emitpropname.vue
|   |-- |-- topperform.vue
|   |-- |-- upgrade.vue
#ไฟล้เหล่านี้คือไฟล์ Component ที่นำไปใช้ในไฟล์ Dashboard
```

Middware file
```bash
|-- middleware/
|   |-- pipad_auth.js
#ไฟล์นี้คือไฟล์ที่เก็บ middleware ที่นำไปใช้ภายในโปรเจ็ค
This file stores middleware used within the project.
```

## Gitlab command

**ชุดคำสั่งสำหรับการใช้งาน Git ในโปรเจ็ค**

คำสั่งสำหรับการ Clone และ Remote Repository

```bash
git clone <link-to-git-repository-clone> 
#คำสั่งเพื่อทำการ clone ไฟล์จาก git
cd <clone-folder>
#คำสั่งเพื่อไปที่ folder ของไฟล์ที่ clone มา
git remote add upstream <link-to-git-repository-remote> 
#คำสั่งเพื่อเพิ่ม remote ไปยัง git 
git remote -v 
#คำสั่งเพื่อเช็คว่ามี remote อะไรบ้างที่เชื่อมต่อ
git remote rm <remote>
#คำสั่งเพื่อลบ remote ที่ต้องการ

```

คำสั่งสำหรับการใช้งาน Branch 

```bash
git branch <Branch-Name> 
#คำสั่งเพื่อทำการสร้าง branch ใหม่
git checkout <Branch-Name>
#คำสั่งเพื่อทำการย้ายไปยัง branch ที่ต้องการ
```

คำสั่งสำหรับการ Add, Commit และ Push ไฟล์

```bash
git add <file-name>
#คำสั่งเพื่อเพิ่มไฟล์ไปยัง staging 
*** OR ***
git add .  
#คำสั่งเพื่อเพิ่มไฟล์ทั้งหมดไปยัง staging
git push <remote> <branch-Name>
#คำสั่งเพื่อส่ง branch รวมถึงไฟล์ไปยัง remote ที่ต้องการ
```

คำสั่งสำหรับการ Pull ไฟล์

```bash
git checkout <branch-name> 
#ก่อนทำการ pull ต้องย้ายไปยัง branch หลักก่อนการ pull เสมอ
git pull <remote> <branch-name>
#คำสั่งเพื่อดึงไฟล์จาก remote มายัง branch ที่ต้องการ
```
