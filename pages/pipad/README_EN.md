# FOR THAI PLEASE PREFERS TO FILE README_TH

## Introduction
This is my Nuxt Project using dependencies and Framework to help me create a progressive web application.

This project contains 

1. Workshop Resume 
    - This workshop that contains data like. name, nickname, email and stuff.
2. Workshop CRUD
    - This is workshop that pull data from API and display data onto Table that can do basic CRUD and have search and pagination features.
3. Workshop Dashboard
    - This is workshop that pull multiple component to show in main template file that can emit and prop data between parent and child then store data into localStorage.
4. Workshop Login
    - This is a workshop that has feature such as using email and password to login and save accessToken into localStorage and verify the token via API to gain access to other pages.


## Setup


Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```


## Development Server
Start the development server on http://localhost:3000

```bash
npm run dev
```


## Framework and Dependency
Framework and Dependency using in this Project

1. [Nuxt](https://nuxt.com/docs/getting-started/installation)
    - Nuxt is a Vue.js framework that provides a higher-level abstraction for building web applications.
2. [Vuetify](https://vuetifyjs.com/en/introduction/why-vuetify/#what-is-vuetify3f)
    - Vuetify is a Material Design component library for Vue.js. It provides a set of components that implement the Material Design specification.
3. [Fontawesome](https://fontawesome.com/docs)
    - FontAwesome is a font and icon library that provides a wide range of icons for web applications.
4. [mdi](https://pictogrammers.com/library/mdi/)
    - mdi is a Material Design icon library for Vue.js. It provides a set of icons that implement the Material Design specification.
5. [chart.js](https://www.chartjs.org/docs/latest/)
    - chart.js is a JavaScript library for creating charts. It provides a wide range of chart types and options.
6. [eslint](https://eslint.org/docs/latest/)
    - eslint is a linting tool for JavaScript. It helps to identify and fix errors in your code.
7. [prettier](https://prettier.io/docs/en/)
    - prettier is a code formatter for JavaScript. It automatically formats your code according to a set of rules.
8. [sass](https://sass-lang.com/documentation/)
    - sass is a CSS preprocessor. It allows you to write CSS code in a more concise and manageable way.
9. [vee-validate](https://vee-validate.logaretm.com/v4/guide/overview/)
    - vee-validate is a validation library for Vue.js. It allows you to validate form inputs.


## Nuxt 3 Minimal Starter
Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.


## Vuetify Starter
Look at the [Vuetify documentation](https://vuetifyjs.com/en/getting-started/installation/) to learn more.


## Environment Variables
In this project, we use environment variables to configure certain aspects. The primary variable is
- **URL_Auth** This variable contains the URL to the API used by the project.


## File Directory and File Guide
>TO ACCESS THE PAGE YOU NEED TO LOGIN USING THE PROVIDED EMAIL AND PASSWORD BELOW VIA LOGIN PAGE FIRST.
> Email : karn.yong@melivecode.com
> Password : melivecode

**File Directory Explanation**
>TO UNDERSTAND WHICH LINE IN THE CODE DO PLEASE PREFER TO READ THE COMMENT INSIDE THE FILE

Crud Table
```bash
|-- pages/
|   |-- pipad/
|       |-- crud_pipad.vue
#This File is for Display and CRUD data from API
``` 

Login Page
```bash
|-- pages/
|   |-- pipad/
|       |-- pipad_login.vue
#This File is for Display Creator's Profile 
```

Resume Page
```bash
|-- pages/
|   |-- pipad/
|       |-- Resume.vue
#This File is for Display Creator's Profile 
```
Dashboard Page
```bash
|-- pages/
|   |-- pipad/
|       |-- work5.vue
#This File is main Template Dashboard Page which use Component from components/pipadcombo
```

Dashboard Page Component
```bash
project-root-directory/
|-- components/
|   |-- pipadcompo/
|   |-- |-- activity.vue
|   |-- |-- channel.vue
|   |-- |-- dashboard.vue
|   |-- |-- emitpropname.vue
|   |-- |-- topperform.vue
|   |-- |-- upgrade.vue
#This Folder Contain Components to use in the Dashboard Page
```

Middware file
```bash
|-- middleware/
|   |-- pipad_auth.js
#This file stores middleware used within the project
```

## Gitlab command

**Basic Git Commond to use in Project**

Clone & Repository Command
คำสั่งสำหรับการ Clone และ Remote Repository

```bash
git clone <link-to-git-repository-clone> 
#a command to start cloning file from git
cd <clone-folder>
#a command to change directory to clone folder
git remote add upstream <link-to-git-repository-remote> 
#a command to add remote to your code
git remote -v 
#a command to to check current remote 
git remote rm <remote>
#a command to to remove the remote you want to remove 

```

Branch & Checkout Command

```bash
git branch <Branch-Name> 
#a command to create new branch 
git checkout <Branch-Name>
#a command to change to your desire branch 
```

Add, Commit & Push Command

```bash
git add <file-name>
#a command to add your file to staging
*** OR ***
git add .  
#a command to add all of your file to staging 
git push <remote> <branch-Name>
#a command to push your branch along with staging file to your remote 
```

Pull Command

```bash
git checkout <branch-name> 
#a command to Checkout to main branch before pull 
git pull <remote> <branch-name>
#a command to pull file from remote to desire branch before 
```

