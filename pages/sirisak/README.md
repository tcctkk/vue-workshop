> > > > > > โปรเจ็กต์นี้เป็นเวอร์ชัน Nuxt.js อาจจะไม่ตรงตามแบบเท่าไรและ
> > > > > > ออกแบบมาสำหรับ nuxt ประกอบด้วย components, Login, Chartist, cv, Workshop1, Workshop2 
และปลั๊กอิน/ส่วนประกอบอื่นๆและรายละเอียดต่างๆได้แก่ >>>>>

|-- Nuxt sirisak Workshop
  |-- Sirisak
    |-- cv.vue
    |-- Login.vue
    |-- README.md
    |-- Workshop1.vue
    |-- Workshop2.vue
        |-- components
            |-- Barchart.vue
            |-- EditableName.vue
            |-- Friends.vue
            |-- Leaderboard.vue
            |-- Myschedule.vue
            |-- Overview.vue
            |-- toolbars.vue

1.components จะมีองค์ประกอบเพื่อจะไปรวมกับ Workshop2 เพื่อไปแสดงผมประกอบด้วย

1.1 Toolbar (<toolbars />): คือส่วนที่อาจจะใช้เพื่อแสดงเครื่องมือหรือการนำทางเช่นเมนูหลัก หรืออื่น ๆ ที่เกี่ยวข้องกับการนำทางในหน้าเว็บ และมีการทำงาน components กับ components อีกด้วย

1.2 Overview (<Overview />): ส่วนที่แสดงข้อมูลภาพรวมหรือข้อมูลหลักเกี่ยวกับหัวข้อหลักของหน้านั้น ๆ

1.3 Barchart (<Barchart />): เป็นส่วนที่แสดงกราฟแท่งหรือข้อมูลในรูปแบบกราฟที่แสดงค่าเปรียบเทียบระหว่างข้อมูลต่าง ๆ

1.4 Myschedule (<Myschedule />): ส่วนที่แสดงข้อมูลตารางเวลาหรือกำหนดการของผู้ใช้

1.5 Leaderboard (<Leaderboard />): ส่วนที่แสดงข้อมูลของ Leaderboard หรือการจัดอันดับของผู้ใช้

1.6 Friends (<Friends />): ส่วนที่แสดงข้อมูลเกี่ยวกับเพื่อนหรือผู้ที่เกี่ยวข้องของผู้ใช้

1.7 EditableName (<EditableName />) ส่วนนี้จะเป็นการ ส่งค่าไปมา เช่นส่งจาก components EditableName ไปยัง components Toolbar ด้วยการใช้ Prop, Emit

และ ทำการเก็บข้อมูลไว้ Local Storage เพื่อจะไปแสดงที่หน้าของ Cv และหน้า Workshop2 อีกด้วย

2.Cv เป็นไฟล์แนะนำตัวเองจะเป็นหน้าหลักของไฟล์ Sirisak นี้และยังมี ปุ๋มไปยังหน้าต่างๆเช่น Workshop1, Workshop2, Login,

3.Workshop1 จะเป็นหน้า C/R/U/D คือ การสร้าง การอ่าน การเพิ่มและการลบ หน้านี้จะมีการ Search data และ Pagination

4.Login จะเป็นหน้าเข้าสู้ระบบที่จะมีการใช้ทั้ง Middleware จะมีชื่อไฟล์ว่า sirisak-auth.js เพื่อดักคนที่ไม่มี Token เข้าไปหน้าต่างๆได้ และ Function Validation เพื่อจะให้มีคุณภาพของระบบ

5.Workshop2 จะเป็นการออกแบบตาม UI เพื่อการใช้งาน และการวาง

### การการพัฒนาต่อเพื่อให้ดียิ่งขึ้น

1.การใช้งาน Components: อธิบายเพิ่มเติมเกี่ยวกับวัตถุประสงค์และการใช้งานของแต่ละ components อย่างละเอียด เช่น ความสำคัญของแต่ละ components และวิธีการใช้งานในส่วนต่าง ๆ ของโปรเจ็กต์

2.ฟังก์ชันและการทำงานที่เกี่ยวข้อง: การอธิบายเพิ่มเติมเกี่ยวกับการทำงานของฟังก์ชันหรือเหตุการณ์ที่เกิดขึ้นในแต่ละหน้า อธิบายการทำงานของฟังก์ชันหรือการเชื่อมโยงกับ components อื่น ๆ ในโปรเจ็กต์

3.การจัดการข้อมูล: อธิบายเกี่ยวกับข้อมูลที่ถูกเก็บไว้ใน Local Storage และวิธีการนำข้อมูลนั้นไปใช้ในหน้าอื่น ๆ ของโปรเจ็กต์

4.การสร้างและออกแบบหน้าเว็บ: อธิบายเกี่ยวกับการสร้างและออกแบบหน้าเว็บแต่ละหน้า เพื่อให้ความเข้าใจว่ามีความสำคัญอย่างไรและวิธีการใช้งาน

5.การทดสอบและการพัฒนา: อธิบายเกี่ยวกับวิธีการทดสอบโปรเจ็กต์ และวิธีการพัฒนาต่อยอดโปรเจ็กต์ให้มีประสิทธิภาพขึ้น

6.การติดตั้งและการเริ่มต้นโปรเจ็กต์: อธิบายขั้นตอนการติดตั้งและเริ่มต้นโปรเจ็กต์ของท่านเพื่อให้ผู้ใช้งานใหม่สามารถเริ่มต้นใช้งานได้อย่างรวดเร็ว

7.ของคำเกริ่นนำ: การเพิ่มข้อมูลเพิ่มเติมเกี่ยวกับโปรเจ็กต์นี้และวัตถุประสงค์ของมันการกำหนดแนวทางหรือเป้าหมายของ
โปรเจ็กต์เพื่อเป็นแรงบันดาลใจแก่ผู้ที่สนใจ

### การใช้งานของ gitlab และขั้นตอนในการทำงานต่างๆ
clone and connect repo
git clone <my-link-to-github-remote-repo> 
cd <เข้าไปที่ folder>
git remote add upstream <tcc-link-to-github-remote-repo> 
git remote -v

สร้าง branch ใหม่ทุกครั้งก่อน commit 
git branch <Branch-Name> //สร้างBranch ใหม่
git checkout <Branch-Name>//ไปยัง Branch ที่ต้องการ

การ push 
git add .
git commit -m "Branch-Name" ก่อน commit ควรจะเสร็จให้ดีก่อน
git push origin <Branch-Name>
จากนั้นตรวจสอบ branch ใน gitlab เลือกbranchที่ pushขึ้นมาและกด create merge requests

การ pull 
git checkout trunk
git pull upstream trunk

และต้องขอขอบคุณเป็นพิเศษสำหรับเจ้าของปลั๊กอินและเส้นAPIเหล่านี้:

- [melivecode](https://melivecode.com/)
- [vee-validate](https://vee-validate.logaretm.com/v4/)
- [vuetify](https://vuetifyjs.com/en/)
- [chartjs](https://www.chartjs.org/)
- [nuxt.js](https://nuxtjs.org)
- [mdi.icon](https://pictogrammers.com/library/mdi/)

## :rocket: เริ่มต้นใช้งาน

ไฟล์ Sirisak สร้างขึ้นบน vuetifyjs, Nuxtjs และ vue.js ในการเริ่มต้นให้ทำตามขั้นตอนต่อไปนี้ในไฟล์ Sirisak Workshop จะมีรายละเอียดต่างๆได้แก่:

1. ดาวน์โหลดโครงการ
2. ตรวจสอบให้แน่ใจว่าคุณได้ติดตั้ง node.js (https://nodejs.org/en/) แล้ว
3. พิมพ์ `npm install` ในโฟลเดอร์ต้นทางที่มี `package.json` อยู่
4. พิมพ์ `npm run dev` เพื่อเริ่มเซิร์ฟเวอร์การพัฒนา

## :cloud: สร้างการตั้งค่า

### การติดตั้ง Nuxt มีวิธีติดตั้งหลายแบบ('https://nuxt.com/docs/getting-started/installation')

`npx nuxi@latest init`

### การติดตั้ง vuetify มีวิธีติดตั้งหลายแบบ('https://vuetifyjs.com/en/getting-started/installation/#using-vite')

`npm create vuetify`

### การติดตั้ง chart.js มีวิธีติดตั้งหลายแบบ('https://www.chartjs.org/docs/latest/getting-started/installation.html')

`npm install chart.js`

### ติดตั้งการอ้างอิง มีวิธีติดตั้งหลายแบบ('https://nuxtjs.ir/guide/installation')

# yarn

`yarn install`

# npm

`npm install`

# pnpm

`pnpm install` --shamefully-hoist

### ให้บริการพร้อมการรีโหลดด่วนที่ http://localhost:3000/

`npm run dev`

## การผลิต

### สร้างแอปพลิเคชันสำหรับการผลิต:

`npm run build`

### Locally preview production build:

`npm run preview`

### Run your tests:

นี้คือส่วนที่คุณอยากติดตั้งเอง

- `ติดตั้ง npm` 
- `ติดตั้ง nuxt`  
- `ติดตั้ง axios` 
- `ติดตั้ง moment` 
- `ติดตั้ง bootstrap-vue` 
- `ติดตั้ง fontawesome` 
- `ติดตั้ง sweetalert2` 
- `ติดตั้ง vue-i18n` 

- โปรดอย่าใช้ปลั๊กอินที่ใช้ jQuery หรือ jQuery เนื่องจากมีทางเลือก Vue มากมาย

สำหรับคำอธิบายโดยละเอียดเกี่ยวกับวิธีการทำงานต่างๆ โปรดดูที่
