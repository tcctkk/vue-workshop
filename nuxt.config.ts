import vuetify from "vite-plugin-vuetify";
export default({
  runtimeConfig: {
    public: {
      URL_Auth: process.env.URL_Auth, //env
    },
  },
  ssr: false,
  typescript: {
    shim: false,
    
  },
  
  app: {
    head: {
      title:
        "MaterialPro Free Nuxt 3 Dashboard",
    },
    
  },
  build: {
    transpile: ["vuetify"],
  },
  nitro: {
    serveStatic: true,
  },
  sourcemap: { server: false, client: false },
  devServerHandlers: [],
  globals: {
    definePageMeta: true,
    navigateTo: true,
    reactive: true,
    defineNuxtRouteMiddleware: true,
    useFetch: true,
    ref: true,
    onMounted: true,
},
});
