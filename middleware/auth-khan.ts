export default defineNuxtRouteMiddleware(async (to, from) => {
  const token = localStorage.getItem("accessToken");

  // Check if token, can't login again
  if (token && to.path === "/Khanjana/login") {
    console.log("User already logged in.");
    return navigateTo("/Khanjana/Khanjana");
  }
  // Check if no token, go to login page
  if (!token && to.path !== "/Khanjana/login") {
    console.log("User not login.");
    return navigateTo("/Khanjana/login");
  }

  // if (token) {
  //   try {
  //     const response = await fetch('https://www.melivecode.com/api/auth/user', {
  //       method: 'GET',
  //       headers: {
  //         'Authorization': `Bearer ${token}`
  //       }
  //     });

  //     if (response.status === 200) {
  //       const userData = await response.json();
  //       console.log('User data:', userData);
  //     } else if (response.status === 401 || response.status === 403) {
  //       console.log('Access token issue. Redirecting to login...');
  //       localStorage.removeItem('accessToken');

  //     } else {
  //       console.error('Error fetching user data. Status:', response.status);

  //     }
  //   } catch (error) {
  //     console.error('Error fetching user data:', error);

  //   }
  // }
});
