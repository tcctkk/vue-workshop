export default defineNuxtRouteMiddleware((to, from) => {
  // เงื่อนไขถ้าไม่มีข้อมูล user ใน localStorage ให้ไปยังหน้า login
  if (!localStorage.getItem("accessToken")) {
    return navigateTo('/kissada/workShop4/login');
  }
  
});
