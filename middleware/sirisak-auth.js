// middleware/auth.js
const accessToken = localStorage.getItem("accessToken");
export default defineNuxtRouteMiddleware(() => {
  // ตรวจสอบว่ามี Access Token ใน localStorage หรือไม่

  if (!accessToken) {
    // ถ้าไม่มี Access Token ให้ Redirect ไปยังหน้า Login
    return navigateTo("/sirisak/login");
  }

  // ถ้ามี Access Token ให้ทำการส่งคำขอ GET ไปยัง URL: https://www.melivecode.com/api/auth/user
  if (accessToken) {
    try {
      const response = fetch("https://www.melivecode.com/api/auth/user", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      if (
        response.status === 401 ||
        response.status === 402 ||
        response.status === 403
      ) {
        // กระทำการต่าง ๆ สำหรับ status code 401 หรือ 402
        // สามารถเข้าถึงข้อมูลของ response ได้เพื่อการประมวลผลเพิ่มเติม
        console.error("Unauthorized:", errorData);
        // และ Redirect ไปยังหน้า Login
        return navigateTo("/sirisak/login");
      }

      // ถ้าผ่านการตรวจสอบให้ดำเนินการต่อไป
    } catch (error) {
      console.error("Error fetching user data:", error);
      return navigateTo("/sirisak/login");
    }
  }
});
