# Kissada Workshop #
My project uses vue and nuxt to create this project. Inside there will be various workshops.

_In order to enter the workshop page, users must log in before being able to enter. There will be ***Username***: "karn.yong@melivecode.com", ***Password***: "melivecode"_
        
 * [ ***Workshop #1 Resume*** ] 
    * It is a beautiful way to design your own resume.
 * [ ***Workshop #2 CRUD*** ] 
    * It creates a CRUD or Create, Read, Update and Delete table that pulls the API from https://melivecode.com.
 * [ ***Workshop #3 Dashboard*** ]
    * It is the creation of website pages according to the UX/UI design that was specified.
    
.
## Built with

[![vue](https://skills.thijs.gg/icons?i=vue)](https://vuejs.org/) 
[![nuxt](https://skills.thijs.gg/icons?i=nuxt)](https://nuxt.com/) 
[![javaScript](https://skills.thijs.gg/icons?i=js)](https://www.javascript.com/)

Built with Vue.js, Vuetify, Nuxt.js, and JavaScript.

อ่านเพิ่มเติมเกี่ยวกับ:

- [Vue.js](https://vuejs.org/)
- [Vuetify](https://vuetifyjs.com/)
- [Nuxt.js](https://nuxt.com/)
- [JavaScript](https://www.javascript.com/)

.
# Getting Started #

## Usage #

To get started with the Kissada Workshop project, follow these steps:

1. **Clone the repository:**
    ```bash
    git clone https://gitlab.com/tcctkk/vue-workshop.git
    cd vue-workshop
    ```

2. **Install Dependencies:**
    ```bash
    # Using yarn
    yarn install

    # Using npm
    npm install

    # Using pnpm
    pnpm install --shamefully-hoist
    ```

3. **Run Development Server:**
    ```bash
    # Using yarn
    yarn dev

    # Using npm
    npm run dev

    # Using pnpm
    pnpm run start
    ```

4. **Open your browser and go to [http://localhost:3000](http://localhost:3000)**

Now you're ready to explore the workshops and contribute to the project!

## Contributing (Git Commands) 
If you have a suggestion that would make this better, please fork the repo and create a pull request.
```bash
   # Fork the Project
1. Fork the Project
   # Clone project
2. git clone <my-link-to-github-remote-repo>
   # Access folder vue-workshop
3. cd <Name-project>
   # Open project
4. code .

   # Connect repo
5. git remote add upstream <link-to-github-remote-repo> 
   # This command lists the remote repositories to which your repository is connected.
6. git remote -v

   # Create branch
7. git branch <Branch-Name> 
   # Go to the desired branch to modify the code. 
8. git checkout <Branch-Name>


### Push ###

   # This command adds files or changes to the working directory in preparation for commit.
10 .git add .
   # This command records changes that are added to the staging area along with a commit message.
11. git commit -m <Branch-Name>
    # This command sends commits and changes to the remote repository.
12. git push origin <Branch-Name>

13. Open a merge requests

### Pull ###

# This command changes the branch you want to work on.
git checkout trunk
#This command pulls changes from the remote repository and merges them into your current branch.
git pull upstream trunk
```



## Folder Structure 
```bash
vue-workshop
    ├── .nuxt
    ├── assets
    ├── component
    ├── layouts
    ├── middleware
    ├── node_modules
    ├── page
    ├── plugins
    ├── public
    ├── theme
    ├── types
    ├── .gitignore
    ├── .npmrc
    ├── app.vue
    ├── error.vue
    ├── nuxt.config.ts
    ├── package-lock.json
    ├── package.json
    ├── README.md
    └── tsconfig.json

```

| name | Description |
|-------|-----------|
|.nuxt|Uses the .nuxt/ directory in development to generate your Vue application.|
|assets|The assets/ directory is used to add all the website's assets that the build tool will process.|
|component|The components/ directory is where you put all your Vue components.|
|layouts|Provides a layouts framework to extract common UI patterns into reusable layouts.|
|middleware|Provides middleware to run code before navigating to a particular route.|
|node_modules|The package manager stores the dependencies of your project in the node_modules/ directory.|
|page|Provides file-based routing to create routes within your web application.|
|plugins|This directory contains plugins, which are used to extend the functionality of Nuxt.js.|
|public|This directory contains static files that are served to the browser, such as the index.html file.|
|theme|This directory contains the application theme.|
|types|Type definition for a theme in a project, which describes the structure and style of a theme used in a Vue.js application.|
|.gitignore|This file tells Git which files to ignore.|
|.npmrc|This file contains configuration settings for npm.|
|app.vue|This is the main Vue component for the application.|
|error.vue|This is the component that is displayed when an error occurs.|
|nuxt.config.ts|This file contains the configuration settings for Nuxt.js.|
|package-lock.json|This file contains the exact versions of all the dependencies that are installed.|
|package.json|This file contains information about the project, such as its name, version, and dependencies.|
|README.md|This file is a Markdown file that describes the project.|
|tsconfig.json|This file contains the configuration settings for TypeScript.|

## My Project Structure ##

```bash
   
components 
│     
└──ui-components
   │     
   └──kissada
      ├── workShop1 # workShop 1 It is a beautiful way to design your own resume.
      │   └── kissadaComponent.vue 
      ├── workShop2 # workShop 2 It creates a CRUD
      │   ├── createTableComponent.vue
      │   ├── editTableComponent.vue
      │   ├── SearchFieldComponent.vue
      │   └── tableAPI.vue
      ├── workShop3 # workShop 3 It is the creation of website pages according to the UX/UI design that was specified.
      │   ├── cardiologist
      │   │   ├── eventsComponent.vue
      │   │   ├── notificationsComponent.vue
      │   │   └── profileCardComponent.vue    
      │   ├── content
      │   │   └── contentComponent.vue
      │   ├── overview
      │   │   ├── overviewComponent.vue
      │   │   └── progressComponent.vue 
      │   ├── patient
      │   │   └── patientAnalyticsComponent.vue
      │   ├── search
      │   │   └── searchComponent.vue
      │   └── editComponent.vue
      └── README.md      

pages
│     
└──kissada    
   ├── editTable 
   │   └── [id].vue 
   ├── workShop1
   │   └── kissada.vue 
   ├── workShop2 
   │   ├── createTable.vue
   │   └── tableAPI.vue
   ├── workShop3 
   │   └── dashboard.vue
   ├── workShop4 
   │   └── login.vue
   └── listWorkshop.vue
```

| name | Description |
|-------|-----------|
|kissadaComponent.vue|This likely refers to a Vue.js component responsible for create resume|
|createTableComponent.vue| This likely refers to a Vue.js component responsible for creating or adding new tables.|
|editTableComponent.vue|This component is probably used for editing existing tables. It might include functionalities like updating table data or modifying table settings|
|tableAPI.vue|This could be a Vue.js component dedicated to handling API (Application Programming Interface) calls related to tables. It might encapsulate functions for fetching, sending, or manipulating data through an API.|
|eventsComponent.vue|This component likely deals with handling and displaying events. It might include features such as event creation, editing, and viewing.|
|notificationsComponent.vue|This is likely a component responsible for managing and displaying notifications. It could handle the logic for showing various types of notifications to users.|
|profileCardComponent.vue |This component probably represents a user profile card, containing information and details about a user.|
|contentComponent.vue|This is a generic name, but it could be a Vue.js component that acts as a container for various types of content.|
|overviewComponent.vue|This might be a component providing a summary or overview of a larger system or dataset.|
|progressComponent.vue |This component likely deals with displaying and managing progress indicators. It could be used to show the progress of tasks or processes.|
|patientAnalyticsComponent.vue|This could be a component specifically designed for displaying analytics or insights related to patient data.|
|searchComponent.vue|This is likely a component dedicated to handling search functionality. It might include a search bar and logic for filtering and displaying search results.|
|editComponent.vue|This is a generic name and could refer to a component used for editing various types of content or data. The specific context would determine its exact functionality.|
